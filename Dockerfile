############################################################
# Dockerfile build AIB Node
# in ubuntu
############################################################
# Base image to ubuntu:16.04
FROM ubuntu:16.04

# Install.
RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get install -y build-essential && \
    apt-get install -y software-properties-common && \
    apt-get install -y curl git nano vim

# Define working directory.
WORKDIR /root

# Install Dependencies
RUN cd /root && \
    git clone https://github.com/iobond/aib.git && \
    apt-get install -y libtool autotools-dev automake pkg-config libssl-dev libevent-dev bsdmainutils && \
    apt-get install -y libboost-system-dev libboost-filesystem-dev libboost-chrono-dev libboost-program-options-dev libboost-test-dev libboost-thread-dev

# Install BerkeleyDB
RUN add-apt-repository -y ppa:bitcoin/bitcoin && \
    apt-get update && \
    apt-get install -y libdb4.8-dev libdb4.8++-dev

# Install SSHD
RUN apt-get update && \
    apt-get install -y openssh-server openssh-client && \
    mkdir /var/run/sshd

# Build AIBD
RUN cd /root/aib && \
    git checkout -b 0.17 origin/0.17 && \
    ./autogen.sh && \
    ./configure && \
    make -j8

# Copy Compile File to .aib
RUN cp /root/aib/src/aibd /root/.aib/ && \
    cp /root/aib/src/aib-cli /root/.aib/ && \
    cp /root/aib/src/aib-tx /root/.aib/

#Add file from dir
COPY ./start.sh /root
COPY ./sshd_config /etc/ssh/sshd_config
COPY ./aib.conf /root/.aib/aib.conf

#Change Access
RUN chmod u+x /root/start.sh

#Open Port
EXPOSE 22
#P2P Port
EXPOSE 31415
#RPC Port
EXPOSE 51413

# Define default command.
CMD ["/bin/bash", "/root/start.sh"]
