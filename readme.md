Clone Dockerfile

    git clone https://botChun@bitbucket.org/botChun/aibd_dockerfile.git


Build Image

    cd aibd_dockerfile
    docker build -t aibd:v1 .



Build Container

    docker run -d -it -p 31022:22 -p 31415:31415 --name aibd aibd:v1  /root/start.sh